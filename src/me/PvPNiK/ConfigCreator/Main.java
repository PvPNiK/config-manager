package me.PvPNiK.ConfigCreator;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

	public static Main instance = null;

	public static Main getInstance() {
		return instance;
	}

	@Override
	public void onEnable() {

		instance = this;
		
		// # Creating all the configs files.
		for (Configs cfg : Configs.values())
			cfg.createFile();

		// # Example of setting data into the config.
		FileConfiguration config = Configs.CONFIG1.getConfig();
		config.set("Example", 1);
		config.set("Another", "Example");
		Configs.CONFIG1.save(config);

	}

}
