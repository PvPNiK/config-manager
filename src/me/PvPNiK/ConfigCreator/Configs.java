package me.PvPNiK.ConfigCreator;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public enum Configs {

	CONFIG1("Config1"), CONFIG2("Config2", "toConfig2/ImHere");

	private String fileName;
	private File file;
	private FileConfiguration config;
	private String path = "";

	private Configs(String fileName) {
		this.setFileName(fileName);
	}

	private Configs(String fileName, String path) {
		this.setFileName(fileName);
		this.setPath(path);
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getPath() {
		return Main.getInstance().getDataFolder() + "/" + path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public YamlConfiguration getConfig() {
		return YamlConfiguration.loadConfiguration(file);
	}

	public void save(FileConfiguration config) {
		try {
			config.save(file);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	public void createFile() {
		
		if (!Main.getInstance().getDataFolder().exists())
			Main.getInstance().getDataFolder().mkdirs();
		
		new File(getPath()).mkdirs();
		file = new File(getPath(), fileName + ".yml");
		
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		config = new YamlConfiguration();
		try {
			config.load(file);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}
	}

}
